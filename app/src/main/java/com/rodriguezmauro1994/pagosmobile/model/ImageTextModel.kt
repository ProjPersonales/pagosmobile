package com.rodriguezmauro1994.pagosmobile.model

import android.graphics.Bitmap

/**
 * Created by ROD
 */
class ImageTextModel(val id: String, val name: String, val thumbnailURL: String) {
    var thumbnailImage: Bitmap? = null
}