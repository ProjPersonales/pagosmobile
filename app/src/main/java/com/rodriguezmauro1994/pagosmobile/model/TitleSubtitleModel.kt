package com.rodriguezmauro1994.pagosmobile.model

/**
 * Created by ROD
 */
class TitleSubtitleModel(val id: String, val title: String, val subtitle: String)