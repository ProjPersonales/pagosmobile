package com.rodriguezmauro1994.pagosmobile.interfaces

interface KeyboardManagerCallback {
    fun setUpNumber(number: String): Boolean
}