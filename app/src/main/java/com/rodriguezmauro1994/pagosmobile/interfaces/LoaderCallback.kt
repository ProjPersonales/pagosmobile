package com.rodriguezmauro1994.pagosmobile.interfaces

interface LoaderCallback {
    fun makeTransaction()
    fun onActionClicked()
}