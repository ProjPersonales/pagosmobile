package com.rodriguezmauro1994.pagosmobile.interfaces

/**
 * Created by ROD
 */
interface TextWatcherCallback {
    fun onTextChanged(value: Double, text: String)
}