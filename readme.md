# Pagos mobile

PagosMobile utiliza las APIs de MercadoPago para procesar un pago con tarjeta de credito.

  - Diseño: https://marvelapp.com/404jca7
  - Source: https://bitbucket.org/ProjPersonales/pagosmobile
  - Trello Board: https://trello.com/b/m1Z5MtgM

[![N|Solid](https://i.imgur.com/ughq7tW.gif)]

## Tecnologías utilizadas

  - Android nativo (Kotlin)
  - Arquitectura MVP
  - Test Driven Development
  - Conectividad mediante Volley
  - Git

### Agradecimientos:
  - Ícono de la app: Prosymbols
  - Credit card holder: Smashicons
  - Card Issuer holder: Freepik
  - Wifi Error: Freepik
  - Not found error: Google

License
----

GPL